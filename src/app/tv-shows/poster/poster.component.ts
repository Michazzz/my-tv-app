import {ChangeDetectionStrategy, Component, Input, OnChanges, ViewEncapsulation} from '@angular/core';
import {Show} from '../tv.models';
import {get} from 'lodash';
import {ActivatedRoute} from '@angular/router';
import {ShowDetailsComponent} from '../show-details/show-details.component';

type PosterSize = 'lg' | 'md';
type ShowImageSize = 'original' | 'medium';

interface PosterSizeDict {
  [size: string]: ShowImageSize;
}

@Component({
  selector: 'ma-poster',
  templateUrl: './poster.component.html',
  styleUrls: ['./poster.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PosterComponent implements OnChanges {

  @Input() show: Show;
  @Input() size: PosterSize = 'md';

  posterUrl: string;

  private readonly placeholder = 'http://fillmurray.com/400/600';

  constructor(private router: ActivatedRoute) { }

  ngOnChanges() {
    const sizeDict: PosterSizeDict = {
      lg: 'original',
      md: 'medium'
    };

    const sizeKey = sizeDict[this.size];

    this.posterUrl = get(this.show, ['image', sizeKey], this.placeholder);
  }

  displayName(): boolean {
    return this.router.snapshot.component !== ShowDetailsComponent;
  }
}
