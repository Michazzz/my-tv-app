import {Injectable} from '@angular/core';
import {ShowDetails} from '../tv.models';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {TvMazeService} from '../tv-maze.service';
import {ShowDetailsParams} from '../tv-shows.module';


@Injectable()
export class ShowDetailsResolver implements Resolve<ShowDetails> {

  constructor(private tv: TvMazeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ShowDetails> {
    const {id} = route.params as ShowDetailsParams;
    return this.tv.getShow(id);
  }

}
