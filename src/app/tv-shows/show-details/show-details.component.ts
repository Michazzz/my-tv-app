import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ShowDetails} from '../tv.models';

@Component({
  selector: 'app-show-details',
  templateUrl: './show-details.component.html',
  styleUrls: ['./show-details.component.scss']
})
export class ShowDetailsComponent implements OnInit {

  show: ShowDetails;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.show = this.route.snapshot.data.show;
  }

}
