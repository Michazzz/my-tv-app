import { Pipe, PipeTransform } from '@angular/core';
import {Episode} from '../tv.models';
import {padStart} from 'lodash';
@Pipe({
  name: 'episodise'
})
export class EpisodisePipe implements PipeTransform {

  transform({number, season}: Episode, lowercase = false): string {
    const [episodePadded, seasonPadded] = [number, season]
      .map(prop => padStart(prop.toString(), 2, '0'));

    return lowercase
      ? `s${seasonPadded}e${episodePadded}`
      : `S${seasonPadded}E${episodePadded}`;
  }

}
