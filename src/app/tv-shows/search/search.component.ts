import { Component } from '@angular/core';
import {TvMazeService} from '../tv-maze.service';
import {Show} from '../tv.models';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {debounceTime, filter, map, startWith} from 'rxjs/operators';
import {tap} from 'rxjs/internal/operators/tap';
import {usernameAvailableValidator} from '../../shared/validators/username-available.validator';
import {merge} from 'rxjs/internal/observable/merge';
import {startsWithUppercaseValidator} from '../../shared/validators/starts-with-uppercase.validator';
import {BookmarksService} from '../../bookmarks/bookmarks.service';
import {Bookmark} from '../../bookmarks/bookmarks.models';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  shows: Show[];
  form: FormGroup;
  click$ = new Subject<void>();
  bookmarks$: Observable<Show[]>;

  constructor(private tvs: TvMazeService,
              private bs: BookmarksService<Show>,
              private fb: FormBuilder) {
    this.bookmarks$ = this.bs.getAll();

    this.form = this.fb.group( {
      query: ['',
        [Validators.required, Validators.minLength(2), startsWithUppercaseValidator()],
        [usernameAvailableValidator()]
      ]}
    );

    const formRequest$ = merge(
      this.form.valueChanges,
      this.form.statusChanges,
      this.click$);

      formRequest$.pipe(
        startWith(1),
        map(() => this.form.value.query),
        debounceTime(500),
        tap(() => this.form.controls.query.errors && console.log(this.form.controls.query.errors)),
        tap(() => this.form.status && console.log(this.form.status)),
        filter(() => this.form.valid)
      ).subscribe(this.search);
  }

  private search = (query: string) => {
    this.tvs.searchShows(query).subscribe(shows => this.shows = shows);
  }
}
