import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TvMazeService} from './tv-maze.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { SearchComponent } from './search/search.component';
import { PosterComponent } from './poster/poster.component';
import { ShowDetailsComponent } from './show-details/show-details.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ErrorInterceptor} from '../shared/interceptors/error.interceptor';
import {RouterModule, Routes} from '@angular/router';
import {ShowDetailsResolver} from './show-details/show-details.resolver';
import { EpisodisePipe } from './pipes/episodise.pipe';
import {BookmarksModule} from '../bookmarks/bookmarks.module';

export interface ShowDetailsParams {
  id: string;
}

const routes: Routes = [];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    BookmarksModule,
  ],
  declarations: [SearchComponent, PosterComponent, ShowDetailsComponent, EpisodisePipe],
  providers: [
    TvMazeService,
    ShowDetailsResolver,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    }
  ]
})
export class TvShowsModule { }
