import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Show, ShowDetails, ShowResponse} from './tv.models';
import {debounceTime, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TvMazeService {

  private readonly apiRoot = 'https://api.tvmaze.com';

  constructor(private http: HttpClient) { }

  private static sortByRating(a: Show, b: Show) {
    if (a.rating.average === null || b.rating.average === null) {
      return 1;
    }
    return (a.rating.average <= b.rating.average ? 1 : -1);
  }

  searchShows(query: string): Observable<Show[]> {
    return this.http.get<ShowResponse[]>(`${this.apiRoot}/search/shows?q=${query}`)
      .pipe(
        map(showResponse => showResponse.map(({show}) => show)),
        map(show => show.sort((a, b) => TvMazeService.sortByRating(a, b)))
      );
  }

  getShow(id: string): Observable<ShowDetails> {
    return this.http.get<ShowDetails>(`${this.apiRoot}/shows/${id}?embed=episodes`).pipe(debounceTime(5000));
  }
}
