import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ContactComponent} from './contact/contact.component';
import {SearchComponent} from './tv-shows/search/search.component';
import {SignedInGuard} from './shared/guards/signed-in.guard';
import {RolesGuard} from './shared/guards/roles.guard';
import {Page404Component} from './page404/page404.component';
import {ShowDetailsComponent} from './tv-shows/show-details/show-details.component';
import {resolve} from 'q';
import {ShowDetailsResolver} from './tv-shows/show-details/show-details.resolver';

const routes: Routes =
  [
    {
      path: '',
      component: HomeComponent
    },
    {
      path: 'tv-shows',
      children: [
        {
          path: '',
          component: SearchComponent
        },
        {
          path: ':id',
          component: ShowDetailsComponent,
          resolve: {
            show: ShowDetailsResolver
          }
        }
      ]
    },
    {
      path: 'contact',
      component: ContactComponent,
      canActivate: [RolesGuard, SignedInGuard],
      data: {
        roles: ['doctor', 'user']
      }
    },
    {
      path: '**',
      component: Page404Component
    }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: false, enableTracing: false})],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
