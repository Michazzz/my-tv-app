import { Component } from '@angular/core';
import {AbstractControl} from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent {

  contact = {
    email: '',
    message: ''
  };

  constructor() { }

  send() {
    console.log('Sending form....');
  }

  showErrors(control: AbstractControl): boolean {
    return (control.dirty || control.touched) && control.invalid;
  }
}
