import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export function startsWithUppercaseValidator(): ValidatorFn {
  return(control: AbstractControl): ValidationErrors | null => {
    const rule = /^[A-Z]/;
    return !control.value || rule.test(control.value[0])
    ? null
      : {
      startsWithUppercase: control.value[0]
      };
  };
}

/*
1. Add parameter called withNumbers and the type of t he parameter should be boolean
2. When withNumbers will be true I would like to
   valid query with numbers for example: 8 mile or Ocean 11
3. When withNumbers will be false I would like to invalid query with numbers so
   8 mile or Ocean 11 will be invalid. Valid query will be for example: Super Man or Shrek.

 */
