import {AbstractControl, AsyncValidatorFn, ValidationErrors} from '@angular/forms';
import {Observable} from 'rxjs';
import {ajax} from 'rxjs/ajax';
import {delay, map} from 'rxjs/operators';
import {tap} from 'rxjs/internal/operators/tap';

export function usernameAvailableValidator(): AsyncValidatorFn {
  return(control: AbstractControl): Observable<ValidationErrors | null> => {
    const apiUrl = 'https://jsonplaceholder.typicode.com/users';

    const validity = users =>
      users.some(user =>
        user.username.toLowerCase() === control.value.toString().toLowerCase());

    const validationResult = invalid => invalid ? {
      usernameAvailable:  true
      }
      : null;

    return ajax(apiUrl)
      .pipe(
        delay(1000),
        tap(response => console.log(response)),
        map(({response}) => response),
        tap(response => console.log(response)),
        map(validity),
        map(validationResult)
      );
  };
}
