import { Directive } from '@angular/core';
import {AbstractControl, AsyncValidator, NG_ASYNC_VALIDATORS, ValidationErrors, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {usernameAvailableValidator} from '../validators/username-available.validator';

@Directive({
  selector: '[appUsernameAvailable]',
  providers: [
    {provide: NG_ASYNC_VALIDATORS, useExisting: UsernameAvailableDirective, multi: true}
  ]
})
export class UsernameAvailableDirective implements AsyncValidator {
  validate(control: AbstractControl): Promise<ValidationErrors | null> | Observable<Validators | null> {
    return usernameAvailableValidator()(control);
  }
}
