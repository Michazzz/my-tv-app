import {Directive, EventEmitter, HostListener, Output} from '@angular/core';
import {NgForm} from '@angular/forms';

@Directive({
  selector: '[appSubmitIfValid]'
})
export class SubmitIfValidDirective {

  // tslint:disable-next-line:no-output-rename
  @Output('appSubmitIfValid') valid = new EventEmitter<void>();

  constructor(private formRef: NgForm) { }

  @HostListener('click')
  handleClick() {
    this.markFieldAsDirty();
    this.emitIfValid();
  }
  private markFieldAsDirty() {
    Object.keys(this.formRef.controls)
      .forEach( fieldName =>
        this.formRef.controls[fieldName].markAsDirty());
  }

  private emitIfValid() {
    if (this.formRef.valid) {
      this.valid.emit();
    }
  }
}

