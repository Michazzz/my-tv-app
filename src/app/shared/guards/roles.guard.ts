import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RolesGuard implements CanActivate {
  canActivate(route: ActivatedRouteSnapshot): boolean {
    const currentRoles = ['user', 'admin', 'vip', 'editor'];
    const requiredRoles = route.data.roles;
    return requiredRoles.some(requiredRole =>
      currentRoles.some(currentRole => currentRole === requiredRole));
    // requiredRole => currentRoles.includes(requiredRole)
  }
}
