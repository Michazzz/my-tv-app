import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SignedInGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const isLoggedIn = prompt('Are you signed in?') === 'yes';
    if (!isLoggedIn) {
      this.router.navigateByUrl('');
    }
    return isLoggedIn;
  }
}
