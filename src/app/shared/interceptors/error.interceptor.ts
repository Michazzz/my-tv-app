import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(
        tap(event => console.log('[Http]', event)),
        catchError(err => {
            const message = {
              0: 'Your internet was not paid!',
              404: 'The resource is not here',
              fallback: 'Unknown evil occurred'
            }[err.status || 'fallback'];

          console.log(message);

          return throwError(err);
        })
      );
  }
}
