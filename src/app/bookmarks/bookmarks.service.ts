import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Bookmark, BookmarkId} from './bookmarks.models';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookmarksService<T extends Bookmark> {

  private items: T[] = [];
  private readonly apiBase = 'http://localhost:3000/bookmarks';
  private items$ = new BehaviorSubject<T[]>([]);

  constructor(private http: HttpClient) {
    this.http.get<T[]>(this.apiBase)
      .subscribe(items => {
        this.items = items;
        this.items$.next(this.items);
      });
  }

  add(item: T): void {
    this.items.push(item);
    this.http.post(this.apiBase, item)
      .subscribe(
        () => {},
        () => {
          this.items = this.items.filter( ({id}) => item.id !== id);
          this.items$.next(this.items);
        }
      );
  }

  remove(id: BookmarkId): void {
    const bookmark = this.items.find(item => item.id === id);
    const bookmarkIndex = this.items.findIndex(idx => idx.id === bookmark.id);

    this.items = this.items.filter(item => item.id !== id);

    this.http.delete(`${this.apiBase}/${id}`)
      .subscribe(
        () => {
          this.items$.next(this.items);
        },
        () => {
          this.items.splice(bookmarkIndex, 0, bookmark);
          this.items$.next(this.items);
        }
      );
  }

  getAll(): Observable<T[]> {
    return this.items$.asObservable();
  }

  has(id: BookmarkId): boolean {
    return this.items.some(item => item.id === id);
  }
}
