import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BookmarksService} from './bookmarks.service';
import { BookmarkAddComponent } from './bookmark-add/bookmark-add.component';
import { BookmarkRemoveComponent } from './bookmark-remove/bookmark-remove.component';
import { BookmarkedDirective } from './bookmarked.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [BookmarkAddComponent, BookmarkRemoveComponent, BookmarkedDirective],
  exports: [BookmarkAddComponent, BookmarkRemoveComponent, BookmarkedDirective],
  providers: [BookmarksService]
})
export class BookmarksModule { }
