import {Directive, ElementRef, HostBinding, HostListener, Input} from '@angular/core';
import {Bookmark} from './bookmarks.models';
import {BookmarksService} from './bookmarks.service';

@Directive({
  selector: '[appBookmarked]',
  exportAs: 'bookmarked'
})
export class BookmarkedDirective {

  // tslint:disable-next-line:no-input-rename
  @Input('appBookmarked') item: Bookmark;

  constructor(private bs: BookmarksService<Bookmark>,
    private el: ElementRef) {
    (el.nativeElement as HTMLElement).classList.add('hello-native');
  }

  @HostBinding('class.bookmarked')
  get isBookmarked() {
    return this.bs.has(this.item.id);
  }

  @HostBinding('class.opaque')
  isOpaque = false;

  @HostListener('click', ['$event.target'])
  toggleOpaque(target: HTMLElement) {
    if (target.tagName === 'IMG') {
      this.isOpaque = !this.isOpaque;
    }
  }
}
