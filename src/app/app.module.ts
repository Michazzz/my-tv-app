import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { ContactComponent } from './contact/contact.component';
import {HttpClientModule} from '@angular/common/http';
import {TvShowsModule} from './tv-shows/tv-shows.module';
import {SubmitIfValidDirective} from './shared/directives/submit-if-valid.directive';
import {FormsModule} from '@angular/forms';
import { UsernameAvailableDirective } from './shared/directives/username-available.directive';
import { Page404Component } from './page404/page404.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    SubmitIfValidDirective,
    UsernameAvailableDirective,
    Page404Component
  ],
  imports: [
    TvShowsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
